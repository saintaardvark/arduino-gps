/* RTC */

/* #include <RtcDS1302.h> */
/* #include <ThreeWire.h> */

/* ThreeWire myWire(4,5,2); // IO, SCLK, CE */
/* RtcDS1302<ThreeWire> Rtc(myWire); */

/* SD card */
#include <SPI.h>
#include <SD.h>
File myFile;

/* GPS */
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// Humidity sensor
#include "Adafruit_Sensor.h"
#include "Adafruit_AM2320.h"

Adafruit_AM2320 am2320 = Adafruit_AM2320();

/* AM 2320 module.  See https://learn.adafruit.com/adafruit-am2320-temperature-humidity-i2c-sensor/pinouts
	 Pins, left to right:

	 VDD - this is power in, can be 3-5VDC
	 SDA - I2C data in/out, requires a pullup of 2-10K to VDD; A4
	 GND - this is signal/power ground
	 SCL - I2C clock in, requires a pullup of 2-10K to VDD; A5
*/

#define SLEEPYTIME 10000

/*
	This sample sketch demonstrates the normal use of a TinyGPS++ (TinyGPSPlus) object.
	It requires the use of SoftwareSerial, and assumes that you have a
	4800-baud serial GPS device hooked up on pins 4(rx) and 3(tx).
*/
/* Is this it? */
/* static const int RXPin = 4, TXPin = 3; */
static const int RXPin = 3, TXPin = 4;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

#define countof(a) (sizeof(a) / sizeof(a[0]))

void setup() {
  Serial.begin(115200);
  ss.begin(GPSBaud);

  Serial.println(F("DeviceExample.ino"));
  Serial.println(F("A simple demonstration of TinyGPS++ with an attached GPS module"));
  Serial.print(F("Testing TinyGPS++ library v. ")); Serial.println(TinyGPSPlus::libraryVersion());
  Serial.println(F("by Mikal Hart"));
  Serial.println();

	Serial.println("Adafruit AM2320 Basic Test");
  am2320.begin();

	/* Rtc.Begin(); */

	/* RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__); */
	/* printDateTime(compiled); */
	Serial.println();

	/* if (!Rtc.IsDateTimeValid()) */
  /*   { */
	/* 		// Common Causes: */
	/* 		//    1) first time you ran and the device wasn't running yet */
	/* 		//    2) the battery on the device is low or even missing */

	/* 		Serial.println("RTC lost confidence in the DateTime!"); */
	/* 		Rtc.SetDateTime(compiled); */
  /*   } */

	/* if (Rtc.GetIsWriteProtected()) */
  /*   { */
	/* 		Serial.println("RTC was write protected, enabling writing now"); */
	/* 		Rtc.SetIsWriteProtected(false); */
  /*   } */

	/* if (!Rtc.GetIsRunning()) */
  /*   { */
	/* 		Serial.println("RTC was not actively running, starting now"); */
	/* 		Rtc.SetIsRunning(true); */
  /*   } */

	/* RtcDateTime now = Rtc.GetDateTime(); */
	/* if (now < compiled) */
  /*   { */
	/* 		Serial.println("RTC is older than compile time!  (Updating DateTime)"); */
	/* 		Rtc.SetDateTime(compiled); */
  /*   } */
	/* else if (now > compiled) */
  /*   { */
	/* 		Serial.println("RTC is newer than compile time. (this is expected)"); */
  /*   } */
	/* else if (now == compiled) */
  /*   { */
	/* 		Serial.println("RTC is the same as compile time! (not expected but all is fine)"); */
  /*   } */

	Serial.print("Initializing SD card...");
	if (!SD.begin(10)) {
		Serial.println("initialization failed!");
	} else {
		Serial.println("worked!");
	}
}

/*

	lat,lng,date,altitude(m),speed(km/h),temp(C),humd(%)
	-48.957427,-123.210647,8/15/2021 18:24:46.00,25.70,37.21,12,-22.20,100.00
*/


void displayInfo() {
	myFile = SD.open("log.csv", FILE_WRITE); /* includes O_APPEND */
	String dataString;
  Serial.print(F("Location: "));
  if (gps.location.isValid()) {
		float lat = gps.location.lat();
		float lng = gps.location.lng();
		Serial.print(lat, 6);
		Serial.print(F(","));
		Serial.print(lng, 6);
		if (myFile) {
			myFile.print(lat, 6);
			myFile.print(F(","));
			myFile.print(lng, 6);
			myFile.print(F(","));
		}
	} else {
		Serial.print(F("NaN"));
		if (myFile) {
			myFile.print(F("nan,nan,"));
		}
	}

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid()) {
		int month = gps.date.month();
		int day = gps.date.day();
		int year = gps.date.year();
		Serial.print(month);
		Serial.print(F("/"));
		Serial.print(day);
		Serial.print(F("/"));
		Serial.print(year);
		if (myFile) {
			myFile.print(month);
			myFile.print(F("/"));
			myFile.print(day);
			myFile.print(F("/"));
			myFile.print(year);
			myFile.print(F(","));
		}
	} else {
		Serial.print(F("NaN"));
		if (myFile) {
			myFile.print(F("nan,"));
		}
	}

  Serial.print(F(" "));
  if (gps.time.isValid()) {
		int hour = gps.time.hour();
		int minute = gps.time.minute();
		int second = gps.time.second();

		if (hour < 10) Serial.print(F("0"));
		Serial.print(hour);
		Serial.print(F(":"));
		if (minute < 10) Serial.print(F("0"));
		Serial.print(minute);
		Serial.print(F(":"));
		if (second < 10) Serial.print(F("0"));
		Serial.print(second);
		if (myFile) {
			if (hour < 10) myFile.print(F("0"));
			myFile.print(hour);
			myFile.print(F(":"));
			if (minute < 10) myFile.print(F("0"));
			myFile.print(minute);
			myFile.print(F(":"));
			if (second < 10) myFile.print(F("0"));
			myFile.print(second);
			myFile.print(F(","));
		} else {
			Serial.print(F("NaN"));
			if (myFile) {
				myFile.print(F("nan,"));
			}
		}

		Serial.print(F("  Altitude (m): "));
		if (gps.altitude.isValid()) {
			float alt = gps.altitude.meters();
			Serial.print(alt);
			if (myFile) {
				myFile.print(alt);
				myFile.print(F(","));
			}
		} else {
			Serial.print(F("NaN"));
			if (myFile) {
				myFile.print(F("nan,"));
			}
		}

		Serial.print(F("  Speed (km/h) : "));
		if (gps.speed.isValid()) {
			float speed = gps.speed.kmph();
			Serial.print(speed);
			if (myFile) {
				myFile.print(speed);
				myFile.print(F(","));
			}

		} else {
			Serial.print(F("NaN"));
			if (myFile) {
				myFile.print(F("nan,"));
			}
		}

		Serial.print(F("  Sats seen : "));
		if (gps.satellites.isValid()) {
			int sats = gps.satellites.value();
			Serial.print(sats);
			if (myFile) {
				myFile.print(sats);
				myFile.print(F(","));
			}
		} else {
			Serial.print(F("NaN"));
			if (myFile) {
				myFile.print(F("nan,"));
			}
		}

		Serial.print(F("  Temp: "));
		float temp = am2320.readTemperature();
		Serial.print(temp);
		if (myFile) {
			myFile.print(temp);
			myFile.print(F(","));
		}
		Serial.print(F("  Hum: "));
		float humid = am2320.readHumidity();
		Serial.print(humid);
		if (myFile) {
			myFile.print(humid);
			/* last value, no trailing comma  */
			/* myFile.print(F(",")); */
		}

		Serial.println();
		Serial.println(dataString);

		if (myFile) {
			// close the file:
			myFile.println();
			myFile.close();
			digitalWrite(LED_BUILTIN, HIGH);
			delay(1000);
			digitalWrite(LED_BUILTIN, LOW);
		}
	}
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms) {
	unsigned long start = millis();
	do {
		while (ss.available())
			gps.encode(ss.read());
	} while (millis() - start < ms);
}

void loop() {
	smartDelay(SLEEPYTIME);
	displayInfo();
	/* logLine(); */
	if (millis() > 5000 && gps.charsProcessed() < 10) {
		Serial.println(F("No GPS detected: check wiring."));
		while(true);
	}
}
